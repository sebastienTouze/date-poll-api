#Funky Framadate API
Experimental REST backend in symfony 4 for Funky framadate frontend.
https://framagit.org/framasoft/framadate/funky-framadate-front

Follow this guide to setup.


## TODO:

return stack of votes when we want to get the configuration of one poll (see [doc/examples.md](doc/examples.md))

***
### Requirements
You MUST have php version 7+ and [Composer](https://getcomposer.org/download/), and a database like Mysql, or postgresql, or mongodb, or any database that Doctrine can handle.
Setup access to database by creating a .env.local file, and fill the database user and password data.
you can check that the database connection works with the command:
```bash  
php bin/console doctrine:schema:validate
```

## Getting started

# Development
install dependencies with Composer


there are examples of request to make it all work in the [doc/examples.md](doc/examples.md).


### install the vendors
```bash
composer install
```
### initiate the database with fixtures
```bash 
php bin/console doctrine:schema:drop --force
php bin/console doctrine:schema:create
php bin/console doctrine:fixtures:load --no-interaction --purge-with-truncate
```
### launch local server with
```bash
php bin/console server:run
```


# Production
set a virtual host on your server, configure CORS access to have the API to work.
configure database access in a .env.local file , replace variables
DATABASE_URL=mysql://database_user:db_user_password@127.0.0.1:3306/database_name
this file is not versionned and should stay like this.

## cronjob to delete expired polls
add this line in your crontab to run the clearance of expired polls everyday at 0h00.
```
0 0 * * * wget http://MYWEBSITE/api/v1/poll/clean-polls 
```
you can open your crontabl in command line with :
```
crontab -e
```
# About

made by B. Lemoine, aka Tykayn, for the framadate funky front end project, a polling libre software.
## contacts
* contact@cipherbliss.com
* https://mastodon.cipherbliss.com/@tykayn
* https://keybase.io/tykayn
* https://twitter.com/tykayn
* https://cipherbliss.com
