<?php


namespace App\Service;


use App\Entity\Comment;
use App\Entity\Owner;
use App\Entity\Poll;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class MailService {


	/**
	 * @var EntityManagerInterface
	 */
	private $em;

	private $mailer;
	/**
	 * @var int
	 */
	private $numSent;

//	public function __construct( EntityManagerInterface $entityManager , Mailer $mailer) {

	private $templating;

	public function __construct(
		EntityManagerInterface $entityManager
	) {
		$this->em = $entityManager;

		// Create the Transport
		$transport = new Swift_SmtpTransport( 'localhost', 25 );

// Create the Mailer using your created Transport
		$mailer = new Swift_Mailer( $transport );

		$this->mailer = $mailer;
	}

	/**
	 * @param Owner $foundOwner
	 * @param Poll|null $poll
	 *
	 * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
	 */
	public function sendCreationMailAction( Owner $foundOwner, Poll $poll = null ) {

		// anti spam , limit to every minute TODO

		$config = [
			'owner'          => $foundOwner,
			'from'           => 'ne-pas-repondre@framadate-api.cipherbliss.com',
			'poll'           => $poll,
			'title'          => 'Création de sondage - ' . ( $poll ? $poll->getTitle() : $poll ),
			'email_template' => 'emails/creation-mail.html.twig',
		];

		return $this->sendMailWithVars( $config );
	}


	/**
	 * anti spam , limit to every minute TODO
	 *
	 * @param Owner $owner
	 *
	 * @return bool
	 */
	public function antispamCheck( Owner $owner ) {

//		$lastSend = $admin_user->getRequestedPollsDate();
//		$now      = new \DateTime();

//		if ( date_diff( $lastSend, $now ) < 60 ) {
//			// too soon!
//			die( 'too soon!' );
//		}
//		$admin_user->setRequestedPollsDate( $now );
//		$em->persist( $admin_user );
//		$em->flush();
		return true;
	}

	/**
	 * send created polls to an owner
	 *
	 * @param Owner $owner
	 *
	 * @return int|void
	 * @throws Exception
	 * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
	 */
	public function sendOwnerPollsAction( Owner $owner ) {

		$config = [
			'owner'          => $owner,
			'title'          => 'Framadate | Mes sondages',
			'email_template' => 'emails/owner-list.html.twig',
		];
		$this->sendMailWithVars( $config );

		return 1;
	}


	/**
	 * @param Comment $comment
	 *
	 * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
	 */
	public function sendCommentNotification( Comment $comment ) {

		$config = [
			'comment'        => $comment,
			'owner'          => $comment->getOwner(),
			'title'          => 'Framadate | commentaire de ' . $comment->getOwner()->getPseudo() . ' _ sondage ' . $comment->getPoll()->getTitle(),
			'email_template' => 'emails/comment-notification.html.twig',
		];

		$this->sendMailWithVars( $config );
	}

	/**
	 * generic way to send email with html template
	 *
	 * @param $config
	 *
	 * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
	 */
	public function sendMailWithVars( $config ) {

		if ( ! $config[ 'poll' ] ) {
			$config[ 'poll' ] = new Poll();
		}
		$emailChoicesTemplates = [
			'creation_poll'       => 'creation-mail.html.twig',
			'edit_poll'           => 'modification-notification-mail.html.twig',
			'creation_poll_admin' => 'author-mail.html.twig',
			'owner_list'          => 'owner-list.html.twig',
			'expiration'          => 'expiration-mail.html.twig',
			'creation_comment'    => 'comment-notification.html.twig',
			'creation_vote'       => 'vote-notification.html.twig',
		];
		$emailChoicesTitles    = [
			'creation_poll'       => 'Framadate | Création de sondage - lien public - ' . $config[ 'poll' ]->getTitle(),
			'edit_poll'           => 'Framadate | Modification de sondage - ' . $config[ 'poll' ]->getTitle(),
			'creation_poll_admin' => 'Framadate | Création de sondage - lien admin - ',
			'owner_list'          => 'Framadate | Vos sondages créés',
			'expiration'          => 'Framadate | Notice d\'expiration du sondage ' . $config[ 'poll' ]->getTitle(),
			'creation_comment'    => 'Framadate | Commentaire de "' . $config[ 'owner' ]->getPseudo() . '" - sondage ' . $config[ 'poll' ]->getTitle(),
			'creation_vote'       => 'Framadate | Vote de "' . $config[ 'owner' ]->getPseudo() . '" - sondage ' . $config[ 'poll' ]->getTitle(),
		];
		// Create a message
		$htmlbody = $this->templating->render(
		// templates/emails/registration.html.twig
			$config[ 'email_template' ],
			$config
		);
		$message  = ( new Swift_Message( 'Wonderful Subject from FAT computer' ) )
			->setContentType( "text/html" )
			->setCharset( 'UTF-8' )
			->setFrom( [ 'contact@framadate-api.cipherbliss.com' ] )
			->setTo( [ 'contact@cipherbliss.com' ] )
			->setBody( $htmlbody, 'text/html' );


// Send the message
		$numSent       = $this->mailer->send( $message );
		$this->numSent = $numSent;
//		$email = ( new TemplatedEmail() )
//			->from( new Address( $config[ 'from' ] ) )
//			->subject( $config[ 'title' ] )
//			->to( $config[ 'owner' ]->getEmail() )
//			->htmlTemplate( $config[ 'email_template' ] )
//			->context( $config );


//		$email = ( new \Swift_Mime_SimpleMessage($config[ 'from' ]) )
//			->setFrom( new Address( $config[ 'from' ] ) )
////			->setHeaders( [new Header('charset', 'UTF-8' )])
//			->setSubject( $config[ 'title' ] )
//			->setTo( $config[ 'owner' ]->getEmail() )
//			->setBody("ble blah bleuh <strong> strong text </strong>    swift mailer","text/html");

//		$email = (new TemplatedEmail())
//			->from('fabien@example.com')
//			->to(new Address('ryan@example.com'))
//			->subject('Thanks for signing up!')
//
//			// path of the Twig template to render
//			->htmlTemplate('emails/footer.html.twig')
//
//			// pass variables (name => value) to the template
//			->context([
//				'expiration_date' => new \DateTime('+7 days'),
//				'username' => 'foo',
//			])
//		;

		// send email


	}
}
