<?php

namespace App\Controller;

use App\Entity\Choice;
use App\Entity\Owner;
use App\Entity\Poll;
use App\Entity\StackOfVotes;
use App\Entity\Vote;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Patch;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/api/v1",name="api_")
 */
class VoteController extends FramadateController {

	/**
	 * add a vote stack on a poll
	 * @Post(
	 *     path = "/poll/{id}/vote",
	 *     name = "new_vote_stack",
	 *     requirements = {"content"="\w+",  "poll_id"="\d+"}
	 * )
	 */
	public
	function newVoteStackAction(
		Poll $poll,
		Request $request
	) {
		if ( ! $poll ) {
			return $this->json( [ 'message' => 'poll not found' ], 404 );
		}

		$em   = $this->getDoctrine()->getManager();
		$data = $request->getContent();
		$data = json_decode( $data, true );


		$emOwner       = $this->getDoctrine()->getRepository( Owner::class );
		$emChoice      = $this->getDoctrine()->getRepository( Choice::class );
		$existingOwner = false;
		$foundOwner    = $emOwner->findOneByEmail( trim( $data[ 'email' ] ) );
		// manage existing or new Owner
		if ( ! $foundOwner ) {
			$foundOwner = new Owner();
			$foundOwner
				->setEmail( $data[ 'email' ] )
				->setPseudo( $data[ 'pseudo' ] );
		} else {
			$existingOwner = true;
		}
		// TODO anti flood
		$foundOwner
			->setModifierToken( $poll->generateAdminKey() );
		$stack = new StackOfVotes();
		$stack
			->setOwner( $foundOwner )
			->setPseudo( $data[ 'pseudo' ] )
			->setPoll( $poll );
		foreach ( $data[ 'votes' ] as $voteInfo ) {

			if ( ! isset( $voteInfo[ 'value' ] ) ) {
				continue;
			}
			$allowedValuesToAnswer = [ 'yes', 'maybe', 'no' ];

			if ( ! in_array( $voteInfo[ 'value' ], $allowedValuesToAnswer ) ) {
				return $this->json( [
					'message'    => 'answer ' . $voteInfo[ 'value' ] . ' is not allowed. should be yes, maybe, or no.',
					'vote_stack' => $stack,
				],
					404 );
			}
			$vote        = new Vote();
			$foundChoice = $emChoice->find( $voteInfo[ 'choice_id' ] );
			if ( ! $foundChoice ) {
				return $this->json( [
					'message'    => 'choice ' . $voteInfo[ 'choice_id' ] . ' was not found',
					'vote_stack' => $stack,
				],
					404 );
			}
			$vote->setPoll( $poll )
			     ->setChoice( $foundChoice )
			     ->setValue( $voteInfo[ 'value' ] );
			$vote->setPoll( $poll );
			$stack->addVote( $vote );
			$poll->addVote( $vote );
			$em->persist( $vote );
			$em->persist( $foundChoice );
		}

		// find poll from choices
		$poll->addStackOfVote( $stack );
		$em->persist( $stack );
		$em->persist( $poll );
		$em->flush();
		$precision = '';
		if ( $existingOwner ) {
			$precision = ' from an existing owner : ' . $foundOwner->getEmail();
		}
		$comments = [];
		$stacks   = [];
		$choices  = [];
		foreach ( $poll->getComments() as $c ) {
			$comments[] = $c->display();
		}
		foreach ( $poll->getStacksOfVotes() as $c ) {
			$stacks[] = $c->display();
		}
		foreach ( $poll->getChoices() as $c ) {
			$choices[] = $c->display();
		}


		if($poll->getMailOnVote()){
			$this->sendVoteNotificationAction($stack->getOwner(), $stack);
		}


		return $this->json( [
			'message'              => 'you created a vote stack' . $precision,
			'poll'                 => $poll,
			'vote_stack'           => $stack->display(),
			'stacks'               => $stacks,
			'comments'             => $comments,
			'choices'              => $choices,
			'choices_count'        => $poll->computeAnswers(),
			'vote_count'           => count( $poll->getStacksOfVotes() ),
			'owner'                => $stack->getOwner(),
			'owner_modifier_token' => $stack->getOwner()->getModifierToken(),
			'admin_key'            => $poll->getAdminKey(),
			'json_you_sent'        => $data,
		],
			201 );
	}

	/**
	 * update vote stack
	 * @Patch(
	 *     path = "/vote-stack/{id}/token/{modifierToken}",
	 *     name = "update_vote_stack",
	 *     requirements = { "id"="\d+"}
	 * )
	 */
	public
	function updateVoteStackAction(
		StackOfVotes $id,
		$modifierToken,
		Request $request
	) {
		$voteStack = $id;
		if ( ! $voteStack ) {
			return $this->json( [ 'message' => 'vote stack not found' ], 404 );
		}
		$poll = $voteStack->getPoll();

		// if only self users are allowed to modify a vote, check it
		if ( ! $modifierToken || $voteStack->getOwner()->getModifierToken() !== $modifierToken ) {
			return $this->json( [ 'message' => 'your token does not allow you to modify this vote ' ],
				403 );
		}
		// everything is ok, we can update all the votes of the vote stack
		//TODO
		// match votes and choices
		// update answers
		// save evrything

		return $this->json( [
			'message'        => 'ok',
			'modifier_token' => $voteStack->getOwner()->getModifierToken(),
			'vote_stack'     => $voteStack->display(),
		],
			200 );


	}

	/**
	 * @Delete(
	 *     path = "/poll/{id}/votes/{accessToken}",
	 *     name = "poll_votes_delete",
	 *     requirements = {"accessToken"="\w+",  "poll_id"="\d+"}
	 * )
	 * @return JsonResponse
	 */
	public
	function deletePollVotesAction(
		Poll $poll,
		$accessToken
	) {
		if ( $accessToken == $poll->getAdminKey() ) {
			$em     = $this->getDoctrine()->getManager();
			$length = count( $poll->getVotes() );
			$em->remove( $poll->getVotes() );
			$em->flush();

			return $this->json( [
				'message' => 'boom! les ' . $length . ' votes du sondage ont été supprimés',
			] );
		} else {
			return $this->json( [
				'message' => 'le token d\'autorisation est invalide, vous ne pouvez pas modifier ce sondage',
			] );
		}
	}
}
