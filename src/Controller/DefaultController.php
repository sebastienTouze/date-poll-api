<?php

namespace App\Controller;

use App\Entity\Owner;
use App\Service\MailService;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use JMS\Serializer\Type\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/api/v1",name="api_")
 */
class DefaultController extends FramadateController {



}
