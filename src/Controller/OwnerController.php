<?php

namespace App\Controller;

use App\Entity\Owner;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;
use JMS\Serializer\Type\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/user",name="user_homepage")
 */
class OwnerController extends FramadateController {
	/**
	 * @Get(path ="/",
	 *      name = "get_default")
	 */
	public function indexAction() {

		return $this->json( [ "message" => "welcome to the framadate user api, ask /api/v1/doc.json for endpoints" ],
			200 );
	}


	/**
	 * Send a mail with all the data to one user
	 * @Get(
	 *     path = "/{email}/polls/send-by-email",
	 *     name = "_polls_send_by_email"
	 * )
	 *
	 * @param string $email
	 *
	 * @return JsonResponse
	 */
	public function sendPollsToUserAction( string $email ) {
		$repository = $this->getDoctrine()->getRepository( Owner::class );


		// find user by email
		$owner = $repository->findOneByEmail( $email );

		if ( $owner ) {
			$templateVars = [
				'owner' => $owner,
				'polls' => $owner->getPolls(),
				'title' => 'Mes sondages - ' . $owner->getEmail(),
			];

			// send email
			$mailSent = 0;
			try {
				$mailSent = $this->sendOwnerPollsAction( $owner );
			} catch ( Exception $e ) {
			} catch ( TransportExceptionInterface $e ) {
			}

			if ( $mailSent ) {
				return $this->json( [
					'message' => 'mail succefully sent to user ' . $owner->getEmail(),
					'data'    => '',
				],
					200 );
			}

			return $this->json( [
				'message' => 'no sucess sending email ' . $owner->getEmail(),
				'data'    => '',
			],
				400 );
		}

		return $this->json( [
			'message' => 'no user found for email ' . $email,
			'data'    => '',
		],
			400 );

	}

}
