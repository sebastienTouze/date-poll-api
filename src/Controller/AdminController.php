<?php

namespace App\Controller;

use App\Entity\Poll;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Route;

/**
 * Class DefaultController
 * @package App\Controller
 * @Route("/admin",name="admin_homepage")
 */
class AdminController extends FramadateController {
	/**
	 * @Get(path ="/",
	 *      name = "_get_default")
	 */
	public function indexAction() {

		return $this->json( [ "message" => "welcome to the framadate admin api, ask /api/v1/doc.json for endpoints" ],
			200 );
	}

	/**
	 * Delete all expired polls and their children
	 * @Get(
	 *     path = "/polls/clean/{token}",
	 *     name = "_clean_expired_polls",
	 * )
	 * token is set up in the main env file
	 */
	public
	function cleanExpiredPolls(
		string $token
	) {
		if ( $this->getParameter( 'ADMIN_TOKEN' ) !== $token ) {
			return $this->json( [
				'message' => 'clean routine can NOT be done, your admin token is bad, and you should feel bad.',
			],
				403 );
		}
		$em     = $this->getDoctrine()->getManager();
		$emPoll = $this->getDoctrine()->getRepository( Poll::class );

		$queryFind   = $em->createQuery(
			'SELECT p
            FROM App\Entity\Poll p
            WHERE p.expiracyDate < CURRENT_DATE()'
		);
		$queryDelete = $em->createQuery(
			'DELETE
            FROM App\Entity\Poll p
            WHERE p.expiracyDate < CURRENT_DATE()'
		);

		$foundPolls = $queryFind->getResult();

		$em->flush();


		return $this->json( [
			'message' => 'clean routine has been done, here are the numbers of polls deleted: ' . count( $foundPolls ),
			'data'    => [
				'count' => count( $foundPolls ),
			],
		],
			200 );
	}

}
