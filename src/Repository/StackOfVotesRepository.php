<?php

namespace App\Repository;

use App\Entity\StackOfVotes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method StackOfVotes|null find($id, $lockMode = null, $lockVersion = null)
 * @method StackOfVotes|null findOneBy(array $criteria, array $orderBy = null)
 * @method StackOfVotes[]    findAll()
 * @method StackOfVotes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StackOfVotesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StackOfVotes::class);
    }

    // /**
    //  * @return StackOfVotes[] Returns an array of StackOfVotes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StackOfVotes
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
