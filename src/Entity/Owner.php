<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OwnerRepository")
 */
class Owner {
	/**
	 * @ORM\Column(type="string", length=255)
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	public $pseudo;
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;
	/**
	 * @ORM\Column(type="string", length=255)
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	public $email;
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Poll", mappedBy="owner",cascade={"persist","remove"},orphanRemoval=true)
	 * @Serializer\Type("App\Entity\Poll")
	 */
	private $polls;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="owner", cascade={"persist","remove"},orphanRemoval=true)
	 */
	private $comments;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\StackOfVotes", mappedBy="owner", cascade={"persist","remove"},orphanRemoval=true)
	 */
	private $stackOfVotes;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $modifierToken;
	/**
	 * @ORM\Column(type="datetime" , options={"default"="CURRENT_TIMESTAMP"})
	 */
	private $createdAt;
	/**
	 * @ORM\Column(type="datetime" , options={"default"="CURRENT_TIMESTAMP"},nullable=true)
	 */
	private $requestedPollsDate;

	public function __construct() {
		$this->polls        = new ArrayCollection();
		$this->comments     = new ArrayCollection();
		$this->stackOfVotes = new ArrayCollection();
		$this->setCreatedAt( new \DateTime() );
		$this->setModifierToken( uniqid() );
	}


	public function getId(): ?int {
		return $this->id;
	}

	public function getEmail(): ?string {
		return $this->email;
	}

	public function setEmail( string $email ): self {
		$this->email = $email;

		return $this;
	}

	public function getPseudo(): ?string {
		return $this->pseudo;
	}

	public function setPseudo( string $pseudo ): self {
		$this->pseudo = $pseudo;

		return $this;
	}

	/**
	 * @return Collection|Poll[]
	 */
	public function getPolls(): Collection {
		return $this->polls;
	}

	public function addPoll( Poll $poll ): self {
		if ( ! $this->polls->contains( $poll ) ) {
			$this->polls[] = $poll;
			$poll->setOwner( $this );
		}

		return $this;
	}

	public function removePoll( Poll $poll ): self {
		if ( $this->polls->contains( $poll ) ) {
			$this->polls->removeElement( $poll );
			// set the owning side to null (unless already changed)
			if ( $poll->getOwner() === $this ) {
				$poll->setOwner( null );
			}
		}

		return $this;
	}

	/**
	 * @return Collection|Comment[]
	 */
	public function getComments(): Collection {
		return $this->comments;
	}

	public function addText( Comment $text ): self {
		if ( ! $this->comments->contains( $text ) ) {
			$this->comments[] = $text;
			$text->setOwner( $this );
		}

		return $this;
	}

	public function removeText( Comment $text ): self {
		if ( $this->comments->contains( $text ) ) {
			$this->comments->removeElement( $text );
			// set the owning side to null (unless already changed)
			if ( $text->getOwner() === $this ) {
				$text->setOwner( null );
			}
		}

		return $this;
	}

	/**
	 * @return Collection|StackOfVotes[]
	 */
	public function getStackOfVotes(): Collection {
		return $this->stackOfVotes;
	}

	public function addStackOfVote( StackOfVotes $stackOfVote ): self {
		if ( ! $this->stackOfVotes->contains( $stackOfVote ) ) {
			$this->stackOfVotes[] = $stackOfVote;
			$stackOfVote->setOwner( $this );
		}

		return $this;
	}

	public function removeStackOfVote( StackOfVotes $stackOfVote ): self {
		if ( $this->stackOfVotes->contains( $stackOfVote ) ) {
			$this->stackOfVotes->removeElement( $stackOfVote );
			// set the owning side to null (unless already changed)
			if ( $stackOfVote->getOwner() === $this ) {
				$stackOfVote->setOwner( null );
			}
		}

		return $this;
	}

	public function getModifierToken(): ?string {
		return $this->modifierToken;
	}

	public function setModifierToken( string $modifierToken ): self {
		$this->modifierToken = $modifierToken;

		return $this;
	}

	public function addComment( Comment $comment ): self {
		if ( ! $this->comments->contains( $comment ) ) {
			$this->comments[] = $comment;
			$comment->setOwner( $this );
		}

		return $this;
	}

	public function removeComment( Comment $comment ): self {
		if ( $this->comments->contains( $comment ) ) {
			$this->comments->removeElement( $comment );
			// set the owning side to null (unless already changed)
			if ( $comment->getOwner() === $this ) {
				$comment->setOwner( null );
			}
		}

		return $this;
	}

	public function getCreatedAt(): ?\DateTimeInterface {
		return $this->createdAt;
	}

	public function setCreatedAt( \DateTimeInterface $createdAt ): self {
		$this->createdAt = $createdAt;

		return $this;
	}

	public function getRequestedPollsDate() {
		return $this->requestedPollsDate;
	}

	public function setRequestedPollsDate( $requestedPollsDate ): self {
		$this->requestedPollsDate = $requestedPollsDate;

		return $this;
	}
}
