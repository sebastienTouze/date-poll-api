<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Comment {
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Owner", inversedBy="comments")
	 * @Serializer\Type("App\Entity\Owner")
	 * @Serializer\Expose()
	 */
	private $owner;

	/**
	 * @ORM\Column(type="text")
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	private $text;

	/**
	 * @ORM\Column(type="datetime")
	 * @Serializer\Type("datetime")
	 * @Serializer\Expose()
	 */
	private $createdAt;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Poll", inversedBy="comments")
	 * @Serializer\Type("App\Entity\Poll")
	 * @Serializer\Expose()
	 */
	private $poll;

	function display() {
		return [
			'id'     => $this->getId(),
			'text'   => $this->getText(),
			'pseudo' => $this->getOwner()->getPseudo(),
			'date'   => $this->getCreatedAt(),
		];
	}

	function __construct() {
		$this->setCreatedAt( new \DateTime() );
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getOwner(): ?Owner {
		return $this->owner;
	}

	public function setOwner( ?Owner $owner ): self {
		$this->owner = $owner;

		return $this;
	}

	public function getText(): ?string {
		return $this->text;
	}

	public function setText( string $text ): self {
		$this->text = $text;

		return $this;
	}

	public function getCreatedAt(): ?DateTimeInterface {
		return $this->createdAt;
	}

	public function setCreatedAt( DateTimeInterface $createdAt ): self {
		$this->createdAt = $createdAt;

		return $this;
	}

	public function getPoll(): ?Poll {
		return $this->poll;
	}

	public function setPoll( ?Poll $poll ): self {
		$this->poll = $poll;

		return $this;
	}
}
