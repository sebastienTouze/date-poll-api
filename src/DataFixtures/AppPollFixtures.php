<?php

namespace App\DataFixtures;

use App\Entity\Choice;
use App\Entity\Comment;
use App\Entity\Owner;
use App\Entity\Poll;
use App\Entity\StackOfVotes;
use App\Entity\Vote;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppPollFixtures extends Fixture {
	public function load( ObjectManager $manager ) {

		/**
		 * create a few demo people
		 */
		$owner = new Owner(); // someone who creates the polls
		$owner->setEmail( 'tktest@tktest.com' )
		      ->setPseudo( 'tk_TEST' );
		$commenterMan = new Owner();
		$commenterMan->setEmail( 'tktest_commentateur@tktest.com' )
		             ->setPseudo( 'tk_TEST_commentateur' );

		$voter = new Owner();
		$voter->setEmail( 'testing_vote_people@tktest.com' )
		      ->setPseudo( 'voting_people_TEST' )
		      ->setModifierToken( uniqid() );
		$manager->persist( $owner );
		$manager->persist( $commenterMan );
		$manager->persist( $voter );
		$manager->flush();


		$poll = new Poll();
		$poll->setTitle( 'citron ou orange' )
		     ->setDescription( 'votre sorbert préféré' )
		     ->setAdminKey( uniqid() )
		     ->setModificationPolicy( 'nobody' );
		$poll->setMailOnVote( true );
		$poll->setOwner( $owner );
		$owner->addPoll( $poll );

		$choiceA = new Choice();
		$choiceA->setName( 'citron' );
		$choiceB = new Choice();
		$choiceB->setName( 'orange' );

		$poll
			->addChoice( $choiceA )
			->addChoice( $choiceB );
		$manager->persist( $poll );

		$stack1 = new StackOfVotes();
		$stack1
			->setPoll( $poll )
			->setOwner( $voter );

		$voteA = new Vote();
		$voteA
			->setPoll( $poll )
			->setStacksOfVotes( $stack1 )
			->setValue( "yes" )
			->setChoice( $choiceA );
		$voteB = new Vote();
		$voteB
			->setPoll( $poll )
			->setStacksOfVotes( $stack1 )
			->setValue( "yes" )
			->setChoice( $choiceB );
		$stack1->setPseudo( 'chuck norris' );
		$manager->persist( $stack1 );

		// voter guy votes again with an other pseudo

		$stack2 = new StackOfVotes();
		$stack2->setPseudo( 'Jean indécis' );
		$stack2
			->setPoll( $poll )
			->setOwner( $voter );

		$voteA = new Vote();
		$voteA
			->setPoll( $poll )
			->setStacksOfVotes( $stack2 )
			->setValue( "maybe" )
			->setChoice( $choiceA );

		$manager->persist( $stack2 );


		$poll = new Poll();

		$ownerComment = new Comment();
		$ownerComment
			->setText( "trop bien ce sondage wohooo! signé l'auteur." )
			->setOwner( $owner );
		$poll->addComment( $ownerComment );

		$someoneComment = new Comment();
		$someoneComment
			->setText( "comme l'auteur se la raconte. PFFFF!" )
			->setOwner( $commenterMan );
		$poll->addComment( $someoneComment );


		$poll->setTitle( 'démo sondage de texte avec deux commentaires' );
		$poll->setDescription( 'description du sondage 2' );

		$poll->setAdminKey( uniqid() );
		$poll->setModificationPolicy( 'self' );
		$poll->setMailOnComment( true );


		$poll->addTextChoiceArray( [ 'un truc', 'deux trucs' ] );
		$poll->setOwner( $owner );
		$owner->addPoll( $poll );

		$manager->persist( $poll );
		$manager->persist( $someoneComment );
		$manager->persist( $ownerComment );


		// voting test with 2 people

		// poll with date type
		$poll      = new Poll();
		$choice    = new Choice();
		$firstDate = new DateTime();
		$choice->setName( $firstDate->format( 'Y-m-d H:i:s' ) );
		$choice2 = new Choice();
		$choice3 = new Choice();
		$choice2->setName( $poll->addDaysToDate( $firstDate, 1 )->format( 'Y-m-d H:i:s' ) );
		$choice3->setName( $poll->addDaysToDate( $firstDate, 2 )->format( 'Y-m-d H:i:s' ) );

		$poll->setTitle( "c'est pour aujourdhui ou pour demain" )
		     ->setDescription( 'Vous avez le choix dans la date' )
		     ->setKind( 'date' )
		     ->setOwner( $owner )
		     ->addChoice( $choice )
		     ->addChoice( $choice2 )
		     ->addChoice( $choice3 )
		     ->setModificationPolicy( 'self' );
		$manager->persist( $poll );

		// poll with cartoon choices
		$poll = new Poll();
		$poll->setTitle( 'dessin animé préféré' )
		     ->setDescription( 'choisissez votre animé préféré' )
		     ->setOwner( $owner )
		     ->setModificationPolicy( 'self' )
		     ->addTextChoiceArray( [
			     "Vic le viking",
			     "Boumbo petite automobile",
			     "Les mystérieuses cités d'or",
			     "Les mondes engloutis",
			     "Foot 2 rue",
			     "Le chat, la vache, et l'océan",
			     "Digimon",
		     ] );

		$someoneComment = new Comment();
		$someoneComment
			->setText( "allez boumbo!" )
			->setOwner( $commenterMan );
		$poll->addComment( $someoneComment );
		$someoneComment2 = new Comment();
		$someoneComment2
			->setText( "je suis pour la team rocket de digimon" )
			->setOwner( $owner );
		$poll->addComment( $someoneComment2 );

		$manager->persist( $poll );

		$stack = new StackOfVotes();
		$stack->setPseudo( 'Wulfila' );
		$stack
			->setPoll( $poll )
			->setOwner( $voter );

		$vote = new Vote();
		$vote
			->setPoll( $poll )
			->setStacksOfVotes( $stack )
			->setValue( "yes" )
			->setChoice( $poll->getChoices()[ 2 ] );
		$vote = new Vote();
		$vote
			->setPoll( $poll )
			->setStacksOfVotes( $stack )
			->setValue( "maybe" )
			->setChoice( $poll->getChoices()[ 1 ] );

		$manager->persist( $stack );

		$stack = new StackOfVotes();
		$stack->setPseudo( 'Tykayn' );
		$stack
			->setPoll( $poll )
			->setOwner( $voter );
		$vote = new Vote();
		$vote
			->setPoll( $poll )
			->setStacksOfVotes( $stack )
			->setValue( "yes" )
			->setChoice( $poll->getChoices()[ 1 ] );
		$vote = new Vote();
		$vote
			->setPoll( $poll )
			->setStacksOfVotes( $stack )
			->setValue( "yes" )
			->setChoice( $poll->getChoices()[ 2 ] );
		$vote = new Vote();
		$vote
			->setPoll( $poll )
			->setStacksOfVotes( $stack )
			->setValue( "no" )
			->setChoice( $poll->getChoices()[ 2 ] );

		$manager->persist( $stack );


		$manager->persist( $commenterMan );

		$manager->flush();
	}
}
