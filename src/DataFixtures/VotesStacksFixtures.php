<?php

namespace App\DataFixtures;

use App\Entity\Owner;
use App\Entity\Poll;
use App\Entity\StackOfVotes;
use App\Entity\Vote;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class VotesStacksFixtures extends Fixture {
	public function load( ObjectManager $manager ) {
		$emPoll = $manager->getRepository( Poll::class );

		$people1 = new Owner();
		$people1->setEmail( 'tktest_nikolas_edison@tktest.com' )
		        ->setPseudo( 'Nikolas Edison' );
		$people2 = new Owner();
		$people2->setEmail( 'wulfila@tktest.com' )
		        ->setPseudo( 'Wulfila' );
		$people3 = new Owner();
		$people3->setEmail( 'billie_jean@tktest.com' )
		        ->setPseudo( 'Billie Jean' );

		// "citron ou orange"
		$poll   = $emPoll->find( 1 );
		$stack1 = new StackOfVotes();
		$stack1
			->setPoll( $poll )
			->setOwner( $people1 );
		$voteA = new Vote();
		$voteA
			->setPoll( $poll )
			->setStacksOfVotes( $stack1 )
			->setValue( "yes" )
			->setChoice( $poll->getChoices()[ 0 ] );
		$voteB = new Vote();
		$voteB
			->setPoll( $poll )
			->setStacksOfVotes( $stack1 )
			->setValue( "maybe" )
			->setChoice( $poll->getChoices()[ 1 ] );
		$poll->addStackOfVote( $stack1 );
		$manager->persist( $poll );
		$manager->persist( $people1 );
		$manager->persist( $stack1 );

		$stack2 = new StackOfVotes();
		$stack2
			->setPoll( $poll )
			->setOwner( $people2 );
		$voteA = new Vote();
		$voteA
			->setPoll( $poll )
			->setStacksOfVotes( $stack2 )
			->setValue( "no" )
			->setChoice( $poll->getChoices()[ 0 ] );
		$voteB = new Vote();
		$voteB
			->setPoll( $poll )
			->setStacksOfVotes( $stack2 )
			->setValue( "yes" )
			->setChoice( $poll->getChoices()[ 1 ] );
		$poll->addStackOfVote( $stack2 );
		$manager->persist( $poll );
		$manager->persist( $stack2 );
		$manager->persist( $people2 );

		// comment on "démo sondage de texte avec deux commentaires"
		$poll = $emPoll->find( 2 );


		// comment on "c'est pour aujourdhui ou pour demain"
		$poll = $emPoll->find( 3 );


		// comment on "dessin animé préféré"
		$poll = $emPoll->find( 4 );

		$stack1 = new StackOfVotes();
		$stack1
			->setPoll( $poll )
			->setOwner( $people1 );
		$voteA = new Vote();
		$voteA
			->setPoll( $poll )
			->setStacksOfVotes( $stack1 )
			->setValue( "maybe" )
			->setChoice( $poll->getChoices()[ 2 ] );
		$voteB = new Vote();
		$voteB
			->setPoll( $poll )
			->setStacksOfVotes( $stack1 )
			->setValue( "maybe" )
			->setChoice( $poll->getChoices()[ 4 ] );
		$poll->addStackOfVote( $stack1 );
		$manager->persist( $poll );
		$manager->persist( $people1 );
		$manager->persist( $stack1 );


		$stack2 = new StackOfVotes();
		$stack2
			->setPoll( $poll )
			->setOwner( $people2 );
		$voteA = new Vote();
		$voteA
			->setPoll( $poll )
			->setStacksOfVotes( $stack2 )
			->setValue( "maybe" )
			->setChoice( $poll->getChoices()[ 3 ] );
		$voteB = new Vote();
		$voteB
			->setPoll( $poll )
			->setStacksOfVotes( $stack2 )
			->setValue( "yes" )
			->setChoice( $poll->getChoices()[ 5 ] );
		$poll->addStackOfVote( $stack2 );
		$manager->persist( $poll );
		$manager->persist( $people2 );
		$manager->persist( $stack2 );


		$stack3 = new StackOfVotes();
		$stack3
			->setPoll( $poll )
			->setOwner( $people3 );
		$voteA = new Vote();
		$voteA
			->setPoll( $poll )
			->setStacksOfVotes( $stack3 )
			->setValue( "yes" )
			->setChoice( $poll->getChoices()[ 1 ] );
		$voteB = new Vote();
		$voteB
			->setPoll( $poll )
			->setStacksOfVotes( $stack3 )
			->setValue( "yes" )
			->setChoice( $poll->getChoices()[ 3 ] );
		$poll->addStackOfVote( $stack3 );
		$manager->persist( $poll );
		$manager->persist( $people3 );
		$manager->persist( $stack3 );

		$manager->flush();
	}
}
