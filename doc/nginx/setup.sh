#!/bib/bash
echo "copy framadate api nginx config"
sudo cp ./framadate-api.conf /etc/nginx/sites-available/

echo "replace api.example.com with your website api domain"
APISUBDOMAIN='other-api-domain.example.com'
read -p 'sub api domain [$APISUBDOMAIN]: ' APISUBDOMAIN
APIDOMAIN='other-api-domain.example.com'
read -p 'sub api domain [$APIDOMAIN]: ' APIDOMAIN
sudo sed -i 's/api.example.com/$APISUBDOMAIN/g' /etc/nginx/sites-available/framadate-api.conf
echo "replace example.com with your website api domain"
sudo sed -i 's/example.com/$APIDOMAIN/g' /etc/nginx/sites-available/framadate-api.conf

echo "enable nginx config"
sudo ln -s /etc/nginx/sites-available/framadate-api.conf /etc/nginx/sites-enabled/framadate-api.conf

echo "testing nginx config"
EXPECTED_NGINX_TEST="nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful"
CHECK_NGINX=$(sudo nginx -t)
# shellcheck disable=SC1073
if [ "$CHECK_NGINX" = "$EXPECTED_NGINX_TEST"]; then
  echo "config is OK"
  exit 0
else
  echo "something is wrong in your nginx config, check the file /etc/nginx/sites-available/framadate-api.conf"
  exit 1
fi
