#!/bin/bash
echo "######################"
echo " time to update the framadate setup"
echo "######################"
git reset --hard
git pull origin master

composer install
php bin/console doctrine:schema:update --force
echo "######################"
echo " update the funky frontend "
echo "######################"
git submodule update
echo "######################"
echo " check dependencies of the frontend with yarn "
echo "######################"
cd funky-framadate-front
yarn
echo "######################"
echo " building the frontend "
echo "######################"
yarn build
echo "######################"
echo " copying built files in the public folder of the symfony project "
echo "######################"
cp -r dist/* public/
echo "######################"
echo " done "
echo "######################"
